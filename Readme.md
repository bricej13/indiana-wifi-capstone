#Archaeological Digital Initiative
##Information Technology Capstone Project 
###Purpose
This is the source code repository for the user interface solution to be developed by the Archaeological Digital Initiative for the BYU Department of Archaeology. We are still in a prototyping stage for this interface, but will update this ReadMe.md to include the specifics of our implementation.
### Goal
The eventual goal is to make the outdoor network easy to set up and for the health of the system to be easily assessed by the users.

