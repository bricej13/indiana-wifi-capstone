import sqlite3, json, urllib2, os, time
from flask import Flask, request, render_template, jsonify, url_for, g, redirect
from discovery import up, getcurdevices, discoveraps

# Configuration
DATABASE = 'database.db'
DEBUG = False
SECRET_KEY = 'da39a3ee5e6b4b0d3255bfef95601890afd80709'
app = Flask(__name__)
app.config.from_object(__name__)

# Open db connection
@app.before_request 
def before_request():
    g.db = sqlite3.connect(app.config['DATABASE'])

# Closd db connection
@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()


@app.route("/")
def hello():
    global DEBUG;
    if request.args.get('debug',''):
        DEBUG = True;
    else:
        DEBUG = False;
    return render_template('template.html')

## get/ips/test
@app.route("/api/get/ips/test")
def getipstest():
    return jsonify(ips={'test1': 'secret', 'test2': 'secret', 'test3': 'secret', 'test4': 'secret'})

## get/ips
@app.route("/api/get/ips")
def getips():
    global DEBUG;
    if DEBUG:
        return jsonify(ips={'test1': 'secret', 'test2': 'secret', 'test3': 'secret', 'test4': 'secret'})
    else:
        return jsonify(ips=getcurdevices())

## get/apipath/ip
@app.route("/api/get/<apipath>/<ip>")
def getapstationtable(apipath, ip):
    # Create cache folder if it doesn't exist
    if not os.path.isdir("cache"):
        os.mkdir("cache")
    cachepath = "cache/" + ip + "." + apipath + ".cache"

    # Return cached file if it exists and is less than 1 min old
    if os.path.isfile(cachepath) and os.path.getmtime(cachepath) > time.time() - 60:
        data = jsonify(json.loads(open(cachepath).read()))
        return data;
    else:
        if ip[:4] == "test":
            data = json.loads(open('test/' + ip).read())

            # Save new cache file
            with open(cachepath, 'w') as cachefile:
                json.dump(data, cachefile)

            return jsonify(data)
        else:
            device = g.db.execute('select * from ips where ip=?', [ip])
            device = device.fetchone()
            post = "client_id=admin&access_token=" + device[1]
            url = "https://" + ip + "/api/v1/" + apipath
            data = json.load(urllib2.urlopen(url, post))

            # Save new cache file
            with open(cachepath, 'w') as cachefile:
                json.dump(data, cachefile)

            return jsonify(data=data)

## set/discover
@app.route("/api/set/discover")
def apdiscover():
    discoveraps()
    return jsonify(data=getcurdevices())

## set/reboot
@app.route("/api/set/reboot/<int:device_id>")
def setreboot(device_id):
    return jsonify(device_id=device_id, cmd='reboot')

## set/txpower
@app.route("/api/set/txpower/<txpower>/<int:device_id>")
def settxpower(txpower,device_id):
    return jsonify(device_id=device_id, txpower=txpower, cmd='txpower')
    return "Setting device %d to power level %s" % (device_id, txpower)

# get/data
@app.route("/api/get/data/<int:device_id>")
def getdata(device_id):
    data = {'resp': ""}
    if device_id != 0:
        data['resp'] = "Getting data for %d" % device_id
        curdevice = g.db.execute('select * from device where id=?', [device_id])

    else:
        data['resp'] =  "Getting data for all APs"
        curdevice = g.db.execute('select * from device where 1')

    result = curdevice.fetchall()
    for device in result:
        data[device[0]] = device

    if request.args.get('force',''):
        data['resp'] += "<br>Force the refresh!"

    return jsonify(data=data)
    

if __name__ == "__main__":
    app.debug = DEBUG
    app.run(host='0.0.0.0')
