CREATE TABLE device (
    id          INTEGER,
    hostname    TEXT,
    ip          TEXT,
    mac         TEXT,
    uptime      TEXT,
    devicetype  INTEGER,

    FOREIGN KEY (devicetype) REFERENCES devicetype (id)
);

CREATE TABLE devicetype (
    id          INTEGER,
    name        TEXT
);
    

CREATE TABLE connection (
    id          INTEGER,
    device1     INTEGER,
    device2     INTEGER,
    signalstrength  INTEGER,

    FOREIGN KEY (device1) REFERENCES device (id),
    FOREIGN KEY (device2) REFERENCES device (id)
);

CREATE TABLE action (
    id          INTEGER,
    displayname TEXT,
    command     TEXT,
    description TEXT,
    devicetype  INTEGER,

    FOREIGN KEY (devicetype) REFERENCES devicetype (id)
);

CREATE TABLE settings (
    id          INTEGER,
    key         TEXT,
    value       TEXT
);

CREATE TABLE ips (
    ip          TEXT
);
