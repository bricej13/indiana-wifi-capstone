from pysnmp.entity.rfc3413.oneliner import cmdgen

# Gets SNMP data for a single ip address
def getapdata(ipaddress):
    cmdGen = cmdgen.CommandGenerator()
    """
    errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
        cmdgen.CommunityData('public'),
        cmdgen.UdpTransportTarget((ipaddress, 161)),
        cmdgen.MibVariable('SNMPv2-MIB', 'system', 0),
        cmdgen.MibVariable('1.3.6.1.2.1.1.5.0')
    )
    """
    errorIndication, errorStatus, errorIndex, varBindTable = cmdGen.nextCmd(
        cmdgen.CommunityData('public'),
        cmdgen.UdpTransportTarget((ipaddress, 161)),
        cmdgen.MibVariable('IP-MIB', 'ipAdEntAddr').loadMibs(),
        maxRows=100,
        ignoreNonIncreasingOid=True
    )
    

    # Check for errors and print out results
    if errorIndication:
        print(errorIndication)
    else:
        if errorStatus:
            print('%s at %s' % (
                errorStatus.prettyPrint(),
                errorIndex and varBinds[int(errorIndex)-1] or '?'
                )
            )
        else:
            # for name, val in varBinds:
            #     print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
            for varBindTableRow in varBindTable:
                for name, val in varBindTableRow:
                    print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))

if __name__ == "__main__":
    getapdata('127.0.0.1')
