import subprocess, sqlite3, json, urllib2, threading

# getcurdevices()
# Returns the list from the database of the currently available access points.
# Removes any entries that aren't available.
# 
# RETURNS: List of ip addresses
lock = threading.RLock()
devices = {}

def getcurdevices():
    global devices
    db = sqlite3.connect('database.db')
    cur = db.cursor()
    cur.execute('select * from ips where 1')
    if cur.rowcount:
        for device in cur.fetchall():
            if up(device[0]):
                devices[device[0]] = device[1]
            else:
                cur.execute('DELETE FROM ips where ip=\'' + device[0] + '\'')
                db.commit()
        db.close()
        return devices
    else:
        return False


# up()
# This function tests whether a network device is 'pingable'
# 
# RETURNS: True if the device is up, False if it is down

def up(ip):
    cmd = "ping -c 1 -w 3 %s" % ip
    ret = subprocess.call(cmd,
        shell=True,
        stdout=open('/dev/null', 'w'),
        stderr=subprocess.STDOUT)
    if ret == 0:
        return True
    else:
        return False

class UpThread(threading.Thread):
    def __init__(self, ip):
        self.ip = ip
        threading.Thread.__init__(self)
    
    def run(self):
        global devices
        if up(self.ip):
            token = gettoken(self.ip)
            if token:
                token = json.load(token)
                if token:
                    with lock:
                        devices[self.ip] = token['access_token']


def discoveraps():
    global devices
    devices = {}
    db = sqlite3.connect('database.db')
    cur = db.cursor()
    cur.execute("DELETE FROM ips WHERE 1;")
    db.commit()

    threads = []
    for i in range(1,20):
        u = UpThread("192.168.1." + str(i))
        threads.append(u)
    for t in threads:
        t.start()
    for t in threads:
        t.join()
    
    cur.executemany("INSERT INTO ips VALUES (?, ?);", tuple(sorted(devices.iteritems())))
    db.commit()
    db.close()
    if devices:
        return True;
    else:
        return False;

def gettoken(ip):
    db = sqlite3.connect('database.db')
    cur = db.cursor()
    post = "grant_type=password&username=admin&client_id=admin&password=admin"
    url = "https://" + ip + "/oauth/authorize"
    try:
        result = urllib2.urlopen(url, post)
    except:
        result = False

    return result

if __name__ == "__main__":
    getcurdevices()


