DROP TABLE IF EXISTS settings;
DROP TABLE IF EXISTS ips;


CREATE TABLE settings (
    id          INTEGER,
    key         TEXT,
    value       TEXT
);

CREATE TABLE ips (
    ip          TEXT,
    token       TEXT
);


INSERT INTO settings VALUES (1, 'baseip', '192.168.1.1');

INSERT INTO ips VALUES ('192.168.1.1', '');
INSERT INTO ips VALUES ('192.168.1.2', '');
INSERT INTO ips VALUES ('192.168.1.117', '');


