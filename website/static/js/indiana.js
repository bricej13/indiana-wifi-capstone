var aps = new Array();
$(document).ready(function() {
    $('.discover_button').click(function() {
        discoveraps()
    });
    $('.refresh_button').click(function() {
        getdata()
    });
    getdata();
});


function getdata() {
    requestcounter = 1;
    $('#loading-icon').html('<img src="/static/img/ajax_circle.gif" />');
    $('#loading-message').html('<span class="text-info">Getting list of access points. </span>');
    $.getJSON("/api/get/ips", function(ipdata) {
    }).done(function(ipdata) {
        requestcounter--;
        if (!$.isEmptyObject(ipdata.ips)) {
            var data = [];
            $('#ap-list').html("")
            $('#device-list').html("")
            $.each(ipdata.ips, function(ip, token) {
                requestcounter++;
                $('#loading-icon').html('<img src="/static/img/ajax_circle.gif" />');
                $('#loading-message').html('<span class="text-info">Getting info from access points. </span>');
                $.getJSON("/api/get/station-table/" + ip, function(apdata) {
                }).done(function(data) {
                    clearalerts();
                    addapinfo(data.data.array)
                }).fail(function(error) {
                    console.log(error);
                    addalert("danger", "Error getting data for Access Point at " + ip + ". <a href='#' onclick='getdata();'>Retry?</a>");
                }).always(function(data) {
                    // Remove loading icon if all requests are done
                    requestcounter--;
                    if (!requestcounter) {
                        $('#loading-icon').html('');
                        $('#loading-message').html('Done. ');
                        setTimeout('$("#loading-message").fadeOut(1000, function() { $("#loading-message").html(""); $("#loading-message").fadeIn();});', 1000);
                    }
                });
            });
        }
        else {
            addalert("danger", "No wireless access points were found on the network. <button class='btn btn-danger btn-xs' onclick='clearalerts(); discoveraps();'> <span class='glyphicon glyphicon-refresh'></span> Search for Access Points</button>");
        }
    }).always(function() {
    });
}

function addapinfo(ap) {
    // Build HTML row
    html = "<a href=\"#\" class=\"list-group-item\">";
    html += "<div class=\"row\">";
    html += "<div class=\"col-xs-4 \" id=\"hostname-" + ap.hostname + "\">" + ap.hostname + "</div>";
    html += "<div class=\"col-xs-4 \" id=\"connectedto-" + ap.hostname + "\">?</div>";
    html += "<div class=\"col-xs-4 \" id=\"strength-" + ap.hostname + "\">?</div>";
    html += "</div>";
    html += "</a>";

    // Add row to panel
    $('#ap-list').append(html);
    
    // Add AP to ap list
    aps[ap.macAddr] = ap.hostname;

    // Remove node if it was added as a WDS link
    // Add AP to graph & ap table (or rename WDS added node)
    if (sys.getNode(ap.macAddr)) {
        n = sys.getNode(ap.macAddr);
        n.data.name = ap.hostname;
    }
    else {
        sys.addNode(ap.hostname, {color:"#00748E", shape:"dot"});
    }

    // Add each of its connected devices
    for (num in ap.stationTable.entries) {
        adddeviceinfo(ap, ap.stationTable.entries[num]);
    }
}

function adddeviceinfo(ap, device) {
    // Fix for devices with 'null' hostname
    if (device.hostname === null)
        device.hostname = "Unknown Device " + device.mac.substr(12,16);

    // Add WDS links between APs
    if (device.deviceType == "WDS Link") {
        // If we know who the two APs are, we can add them
        linkapname = getwdslinkap(device.mac);
        if (linkapname) {
            // Update edges if they already exist
            var edges = sys.getEdges(linkapname, ap.hostname);
            if (edges.length > 0) {
                for (var i = 0; i < edges.length; i++) {
                    edges[i].data.strength = convertss(device.rssi);
                }
            }
            else {
                // Add nodes and edges to graph
                sys.addNode(linkapname, {color:"#00748E", shape:"dot"});
                sys.addEdge(ap.hostname, linkapname, {strength: convertss(device.rssi)});
            }
            // Add link info to table
            if ($('#connectedto-' + linkapname).text() == "?") {
                $('#connectedto-' + linkapname).text("");
                $('#strength-' + linkapname).text("");
                $('#connectedto-' + linkapname).append(ap.hostname + " ");
                $('#strength-' + linkapname).append(convertss(device.rssi, "utf"));
            }
            else {
                $('#connectedto-' + linkapname).append("<br>" + ap.hostname);
                $('#strength-' + linkapname).append("<br>" + convertss(device.rssi, "utf"));
            }
            if ($('#connectedto-' + ap.hostname).text() == "?") {
                $('#connectedto-' + ap.hostname).text("");
                $('#strength-' + ap.hostname).text("");
                $('#connectedto-' + ap.hostname).append(linkapname);
                $('#strength-' + ap.hostname).append(convertss(device.rssi, "utf"));
            }
            else {
                $('#connectedto-' + ap.hostname).append("<br>" + linkapname);
                $('#strength-' + ap.hostname).append("<br>" + convertss(device.rssi, "utf"));
            }
        }
        else {
            // It is OK if it doesn't find the AP it's linked to. When that AP is scanned it will make the link
        }
        return;
    }
    // build HTML row
    html = "<a href=\"#\" class=\"list-group-item\">";
    html += "<div class=\"row\">";
    html += "<div class=\"col-xs-4\">" + device.hostname + "</div>";
    html += "<div class=\"col-xs-4\">" + device.timeAssoc + "</div>";
    html += "<div class=\"col-xs-4\">" + convertss(device.rssi, 'utf') + "</div>";
    html += "</div>";
    html += "</a>";
    
    // Add row to device panel
    $('#device-list').append(html);
    
    // Add node & edge to graph
    sys.addNode(device.hostname, {color:"#00B7E0"});
    sys.addEdge(ap.hostname, device.hostname, {strength: convertss(device.rssi)});
}

function discoveraps() {
    $('#loading-icon').html('<img src="/static/img/ajax_circle.gif" />');
    $('#loading-message').html('<span class="text-info" Searching for new access points. </span>');
    $.getJSON("/api/set/discover" , function(data) {
    }).done(function(data) {
        // addalert("success", "Network devices found, acquiring data. . .");
        getdata();
    }).fail(function(error) {
        addalert("danger", "Error discovering network.");
        $('#loading-icon').html('');
        $('#loading-message').html('<span class="text-danger">Error. </span>');
    }).always(function(data) {
    });
}


// Ads an alert into the page
// type = one of the following ["success", "info", "warning", "danger"]
// message = "This is the message that will be shown
// returns the id of the alert
function addalert(type, message) {
    id = type + Date.now();
    html = '<div id="'+ id +'" class="alert alert-' + type + ' alert-dismissable">';
    html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    html += message;
    html += '</div>';
    $('#alert-area').append(html);
    return id;
}

function clearalerts() {
    $('#alert-area').html("");
}

function convertss(ss, type) {
    var bars = 0;
    if (ss > -60)
        bars = 5;
    else if (ss > -73)
        bars = 4;
    else if (ss > -85)
        bars = 3;
    else if (ss > -98)
        bars = 2;
    else if (ss > -110)
        bars = 1;
    else
        bars = 0;
    if (type == "html")
        return "<span class=\"signal signal" + bars + "\"></span>";
    else if (type == "utf") {
        //$('#connectedto-' + linkapname).append(": &#x2588;&#x2588;&#x2588;&#x2591;&#x2591;");
        utf = "<span class=\"utfbars bars" + bars + "\">";
        for (var i = 1; i <= 5; i++) {
            if (i <= bars)
                utf += "&#x2588;";
            else
                utf += "&#x2591;";

        }
        return utf + "</span>";
    }
    else
        return bars;
}

function getwdslinkap(wdsmac) {
    wdsmactail = parseInt(wdsmac.substr(-5,2) + wdsmac.substr(-2,2), 16);
    for (var apmac in aps) {
        apmactail = parseInt(apmac.substr(-5,2) + apmac.substr(-2,2), 16);
        if (Math.abs(wdsmactail-apmactail) < 32) {
            return aps[apmac];
        }
    }
    return false;
}
