
var Renderer = function(canvas){
    var dom = $('#graph-area');
    var canvas = dom.get(0);
    var ctx = canvas.getContext("2d");
    var gfx = arbor.Graphics(canvas)
    var sys = null

    var _vignette = null
    var selected = null,
        nearest = null,
        _touchP = null;

    
    var that = {
      init:function(pSystem){
        sys = pSystem
        sys.screen({size:{width:dom.width(), height:dom.height()},
                    padding:[36,60,36,60]})

        $(window).resize(that.resize)
        that.resize()
        that._initMouseHandling()

      },
      resize:function(){
        canvas.width = $('#main-container').width()
        canvas.height = .75* $(window).height()
        sys.screen({size:{width:canvas.width, height:canvas.height}})
        _vignette = null
        that.redraw()
      },
      redraw:function(){
        gfx.clear()
        sys.eachEdge(function(edge, p1, p2){
          if (edge.source.data.alpha * edge.target.data.alpha == 0) return
          if (edge.data.strength >= 4)
            linecolor = "green";
          else if (edge.data.strength >= 2)
            linecolor = "#F4BA4D"
          else
            linecolor = "#DA3B3A"
          strength = edge.data.strength * 3 + 1;
            
          gfx.line(p1, p2, {stroke:linecolor, width:strength, alpha:.5})
        })
        sys.eachNode(function(node, pt){
          var w = Math.max(80, 20+gfx.textWidth(node.name) )
          if (node.data.alpha===0) return
          if (node.data.shape=='dot'){
            gfx.oval(pt.x-w/2, pt.y-w/2, w, w, {fill:node.data.color, alpha:node.data.alpha})
            gfx.text(node.name, pt.x, pt.y+7, {color:"white", align:"center", font:"Arial", size:14})
            gfx.text(node.name, pt.x, pt.y+7, {color:"white", align:"center", font:"Arial", size:14})
          }else{
            gfx.rect(pt.x-w/2, pt.y-8, w, 20, 4, {fill:node.data.color, alpha:node.data.alpha})
            gfx.text(node.name, pt.x, pt.y+9, {color:"white", align:"center", font:"Arial", size:12})
            gfx.text(node.name, pt.x, pt.y+9, {color:"white", align:"center", font:"Arial", size:12})
          }
        })
      },
      

      switchMode:function(e){
        if (e.mode=='hidden'){
          dom.stop(true).fadeTo(e.dt,0, function(){
            if (sys) sys.stop()
            $(this).hide()
          })
        }else if (e.mode=='visible'){
          dom.stop(true).css('opacity',0).show().fadeTo(e.dt,1,function(){
            that.resize()
          })
          if (sys) sys.start()
        }
      },
      
      switchSection:function(newSection){
        var parent = sys.getEdgesFrom(newSection)[0].source
        var children = $.map(sys.getEdgesFrom(newSection), function(edge){
          return edge.target
        })
        
        sys.eachNode(function(node){
          if (node.data.shape=='dot') return // skip all but leafnodes

          var nowVisible = ($.inArray(node, children)>=0)
          var newAlpha = (nowVisible) ? 1 : 0
          var dt = (nowVisible) ? .5 : .5
          sys.tweenNode(node, dt, {alpha:newAlpha})

          if (newAlpha==1){
            node.p.x = parent.p.x + .05*Math.random() - .025
            node.p.y = parent.p.y + .05*Math.random() - .025
            node.tempMass = .001
          }
        })
      },
      
      
      _initMouseHandling:function(){
        // no-nonsense drag and drop (thanks springy.js)
        selected = null;
        nearest = null;
        var dragged = null;
        var oldmass = 1

        var _section = null

        var handler = {
          moved:function(e){
            var pos = $(canvas).offset();
            _touchP = arbor.Point(e.originalEvent.touches[0].pageX-pos.left, e.originalEvent.touches[0].pageY-pos.top)
            nearest = sys.nearest(_touchP);

            return false
          },
          clicked:function(e){
            var pos = $(canvas).offset();
            _touchP = arbor.Point(e.originalEvent.touches[0].pageX-pos.left, e.originalEvent.touches[0].pageY-pos.top)
            nearest = dragged = sys.nearest(_touchP);
            
            if (nearest && selected && nearest.node===selected.node){
              return false
            }
            
            
            if (dragged && dragged.node !== null) dragged.node.fixed = true

            $(canvas).unbind('touchmove', handler.moved);
            $(canvas).bind('touchmove', handler.dragged)
            $(window).bind('touchend', handler.dropped)

            return false
          },
          dragged:function(e){
            var old_nearest = nearest && nearest.node._id
            var pos = $(canvas).offset();
            // var s = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)
            var s = arbor.Point(e.originalEvent.touches[0].pageX-pos.left, e.originalEvent.touches[0].pageY-pos.top)

            if (!nearest) return
            if (dragged !== null && dragged.node !== null){
              var p = sys.fromScreen(s)
              dragged.node.p = p
            }

            return false
          },

          dropped:function(e){
            if (dragged===null || dragged.node===undefined) return
            if (dragged.node !== null) dragged.node.fixed = false
            dragged.node.tempMass = 1000
            dragged = null;
            $(canvas).unbind('touchmove', handler.dragged)
            $(window).unbind('touchend', handler.dropped)
            $(canvas).bind('touchmove', handler.moved);
            _touchP = null
            return false
          }


        }

        $(canvas).on('touchstart', handler.clicked);
        $(canvas).on('touchmove', handler.moved);

      }
    }
    
    return that
  }    

  function pruneAllEdges() {
      sys.eachNode(function(node) {
          sys.pruneNode(node);
      });

  }

var sys = arbor.ParticleSystem(1000, 600, 0.5) // create the system with sensible repulsion/stiffness/friction
    sys.parameters({gravity:true}) // use center-gravity to make the graph settle nicely (ymmv)
    sys.renderer = Renderer("#graph-area") // our newly created renderer will have its .init() method called shortly by sys...

    // add some nodes to the graph and watch it go...
    /*
    sys.addNode('alpha', {color:"red", shape:"dot"});
    sys.addNode('bravo', {color:"blue", shape:"dot"});
    sys.addNode('charlie', {color:"green", shape:"dot"});
    sys.addNode('alpha1', {color:"orange", alpha:1});
    sys.addNode('alpha2', {color:"orange", alpha:1});
    sys.addNode('bravo1', {color:"orange", alpha:1});
    sys.addNode('bravo2', {color:"orange", alpha:1});
    sys.addNode('charlie1', {color:"orange", alpha:1});

    sys.addEdge('alpha', 'bravo');
    sys.addEdge('bravo', 'charlie');
    sys.addEdge('alpha', 'alpha1');
    sys.addEdge('alpha', 'alpha2');
    sys.addEdge('bravo', 'bravo1');
    sys.addEdge('bravo', 'bravo2');
    sys.addEdge('charlie', 'charlie1');
    */

